package interfaces.view;
import interfaces.model.Player;
import interfaces.plugins.GamePlugin;
import interfaces.plugins.UIPlugin;

import java.util.List;

import javax.swing.JTextField;



/**
 * This interface is used to hold the UI for the game as well as
 * define the high-level game logic. 
 * 
 * It communicates with the GamePlugin to update the game's state appropriately.
 * 
 * It communicates with the UIPlugin to redraw the UI at the correct time with the current state.
 * 
 * GameFrame holds an instance of the game state.
 * Both the game and UI plugins read the UI state.
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public interface GameFrame{
	
	/**
	 * A list of players that are currently playing the game
	 * 
	 */
	List<Player> players = null;
	
	
	/**
	 * This is an instance of the input field that is required.
	 * It is responsible for getting ALL user input necessary to play the game.
	 * This input will be passed into the current GamePlugin to be parsed and to act upon the
	 * current state
	 * 
	 */
	JTextField commandInput = null;
	
	/**
	 * This method is responsible for running the higher level game logic as follows:
	 * 
	 * setup() from {@link GamePlugin}
	 * 
	 * Loop the following:
	 * 
	 * 	Player p = whosTurn() from {@link GamePlugin}
	 * 
	 * 	if(null == p)
	 * 		cleanUp() from {@link GamePlugin}
	 * 	else
	 * 		runTurn() from {@link GamePlugin}
	 * 
	 * 	if(gameOver()) from {@link GamePlugin}
	 * 		endGame() from {@link GamePlugin}
	 *		redraw() from {@link UIPlugin}
	 * 		break
	 * 
	 * redraw() from {@link UIPlugin}
	 * 
	 * 
	 */
	void run();
	

}
