package interfaces.model;


/**
 * This class defines the specifics for a board that has generally 4 or 8 
 *   adjacent locations. A board that has at most 8 adjacent locations simply allows
 *   for movement along the diagonals of a 4-adjacent board.
 *   
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public interface SquareBoard extends GameBoard {

}
