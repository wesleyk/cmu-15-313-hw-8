package interfaces.model;

import java.util.List;

public interface GameState {


	public GameBoard getGameBoard();
	
	
	public List<Player> getPlayers();
	
	
	/**
	 * Finds and returns the list of pieces that this player has control over.
	 * @param playerName the name of the Player
	 * @return the list of {@link Position} that this player currently controls
	 */
	public List<Position> getPositionsForPlayer(String playerName);


	/**
	 * Finds and returns the list of pieces that this this has control over.
	 * @param teamNumber the team number
	 * @return the list of {@link Position} that this player currently controls
	 */
	public List<Position> getPositionsForTeam(int teamNumber);
}
