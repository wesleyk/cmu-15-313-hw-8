package interfaces.plugins;

import interfaces.model.GameBoard;
import interfaces.model.GameState;
import interfaces.model.Player;
import interfaces.model.Position;
import interfaces.view.panels.InfoPanel;
import interfaces.view.panels.InstructionsPanel;

/**
 * An implementation of the GamePlugin interface represents the rules and 
 *   dynamics of how a specific board game plays.
 *   
 * The game implements methods that define how the model for the game changes
 *   and how it is initially setup. This plugin gets "called-into" by the 
 *   framework because it is a plugin, and should never make a "call-out" for
 *   the same reason.
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public interface GamePlugin {

	public void setMaxTeams(int maxTeams);

	public int getMaxTeams();
	
	public void setMinTeams(int minTeams);

	public int getMinTeams();
	
	public void setMaxPlayers(int maxPlayers);

	public int getMaxPlayers();
	
	public void setMinPlayers(int minPlayers);

	public int getMinPlayers();

	public void setMaxPerTeam(int maxPerTeam);

	public int getMaxPerTeam();
	
	public void setMinPerTeam(int minPerTeam);

	public int getMinPerTeam();
	
	public void setComputerTypes(String[] computerTypes);

	public String[] computerTypes();
	
	/**
	 * This method is called when a user clicks on a position 
	 * in the UI. The position is passed to the method, and the
	 * method should build and return an info panel based 
	 * on the state of the position.
	 * @param position The position that has been clicked.
	 * @return Information Panel about the position.
	 */
	
	public InfoPanel makePositionInfoPanel(Position position);
	
	/**
	 * Given the current state, returns an information panel.
	 * The information panel can be created by initializing a 
	 * a new information panel and adding elements to it. See
	 * the {@link InfoPanel} documentation for information
	 * on adding elements.
	 * @param currentState The current state of the game.
	 * @return Information Panel about the current state.
	 */
	
	public InfoPanel makeGameInfoPanel(GameState currentState);
	
	/**
	 * Givne the current state, returns a panel that gives the player
	 * instructions. The instructions panel should be used guide players
	 * in the construction of their command inputs into the text input
	 * field.
	 * @param currentState The current state of the game
	 * @return An Instructions Panel detailing instructions for the
	 * user input.
	 */
	
	public InstructionsPanel makeInstructionsPanel(GameState currentState);
	
	/**
	 * Initializes the board by returning a 2-D
	 * array of positions which contain the terrain
	 * type of each position, the Piece at each
	 * position (without images assigned), and the position's
	 * point.
	 * @return The initial board.
	 /* initState contains players after lobby*/
	public Position[][] setupBoard(Player[] players);
	
	/**
	 * Applies the given command sequence to the current game, changing the 
	 *   state of the game and returning the new state.
	 * 
	 * @param commandInput The {@link String} that the user inputs describing
	 *   the desired game action. This would be somehow parsed by the specific
	 *   instance of the {@link GamePlugin} in this function and would result 
	 *   in the change in the returned {@link GameBoard}.
	 * @param currentBoard the current state of the game.
	 * @return the new state of the game. If you return null, that will be seen
	 * 			as the user having put in an illegal command and we will just
	 * 			wait for the user to input another command.
	 */
	public GameBoard runTurn(String commandInput, GameState currentState);
	
	/**
	 * Returns the {@link Player} who has the ability to make the next
	 *   move. Simply put, who's turn it is. 
	 * @return the player who's turn it is. If it is a cleanup turn where the 
	 *   game is supposed to manage game functions, returns null. This cleanup
	 *   turn would be automatically called by the framework.
	 */
	public Player whosTurn(GameState currentState);

	/**
	 * That returns an empty array if the game is over but there are no winners, 
	 * otherwise it
	 * returns an array of the team numbers defining who has won the game.
	 * Null if game is not over.
	 * @return empty if the game is not over. Otherwise contains the integers
	 *   representing the teams that have won.
	 */
	public int[] gameOver(GameState currentState);
	
	/**
	 * Taking the current instance of the game, runs any desired alterations
	 *   that happen not at command time. For instance if pieces only die when
	 *   all the players have moved, then when this function is then called, 
	 *   all pieces that are dead would be handled.
	 * @param currentBoard the current board.
	 * @return the board that results from any cleanup functions being run.
	 */
	public GameBoard cleanUp(GameState currentState);
}

