package implementations.model;

import interfaces.model.Position;
import interfaces.model.SquareBoard;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;


/**
 * This class defines the specifics for a board that has generally 4 or 8 
 *   adjacent locations. A board that has at most 8 adjacent locations simply allows
 *   for movement along the diagonals of a 4-adjacent board.
 *   
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public class SquareBoardImpl implements SquareBoard {

	private int height;
	private int width;
	private Position[][] board;

	public SquareBoardImpl(Position[][] board, int width, int height){
		this.height = height;
		this.width = width;
		this.board = board;
	}
	
	@Override
	public Position[][] getBoard() {
		return board;
	}
	
	public void setBoard(Position[][] newBoard){
		this.board = newBoard;
	}

	@Override
	public String pointToUID(Point point) {
		//TODO Make this a more usable UID in the future
		return point.x + "," + point.y;
	}

	@Override
	public Point pointFromUID(String uid) {
		//TODO Make this a more usable UID in the future
		String[] splitUid = uid.split(",");
		return new Point(Integer.parseInt(splitUid[0]), Integer.parseInt(splitUid[1]));
	}

	@Override
	public void addNewPosition(Position pos) {
		board[pos.getPoint().x][pos.getPoint().y] = pos;
	}

	@Override
	public List<Position> getPositionsWithPieces() {

		List<Position> ret = new ArrayList<Position>();
		
		for(int x = 0; x<height; x++){
			for(int y = 0; y<width; y++){
				if(board[x][y] != null && board[x][y].getPiece() != null)
					ret.add(board[x][y]);
			}
		}
		
		return ret;
	}

	@Override
	public List<Point> getAdjacent(Point srcPoint) {
	
		if(srcPoint.x < 0 || srcPoint.x >= height || srcPoint.y < 0 
				|| srcPoint.y >= width)
			return null;
		
		List<Point> ret = new ArrayList<Point>();
		
		for(int x = srcPoint.x-1; x<= srcPoint.x+1; x++){
			for(int y = srcPoint.y-1; y<= srcPoint.y+1; y++){
				/* Make sure that the current point is in bounds*/
				if(x >= 0 && x < height && y >= 0 && y < width){
					ret.add(new Point(x, y));
				}
				
			}
		}
		
		ret.remove(srcPoint);
		
		return ret;	
	}

	@Override
	public Position getPositionAt(Point point) {
		if(point.x < 0 || point.x >= height){
			return null;
		}
		
		if(point.y < 0 || point.y >= width){
			return null;
		}
		
		return board[point.x][point.y];
	}

	@Override
	public int getWidth() {
		return this.width;
	}


	@Override
	public int getHeight() {
		return this.height;
	}


}
