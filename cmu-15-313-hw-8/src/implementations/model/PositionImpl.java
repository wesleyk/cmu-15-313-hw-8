package implementations.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import interfaces.model.*;
import interfaces.plugins.UIPlugin;

public class PositionImpl extends JLabel implements Position {

	private boolean updateView = true;
	
	private Point point = null;
	private Piece piece = null;
	private String terrainType = null;
	private BufferedImage terrain = null;
	
	private UIPlugin uiPlugin = null;
	
	private final int BOARD_LENGTH = 550;
	
	public PositionImpl(Piece piece, Point point, String terrain)
	{
		super();
		
		setBorder(BorderFactory.createLineBorder(Color.black));
		
		this.piece = piece;
		this.point = point;
		this.terrainType = terrain;
	}
	
	public void setPiece(Piece piece) {
		this.piece = piece;
		updateView = true;
	}

	public Piece getPiece() {
		return piece;
	}

	public Point getPoint() {
		return point;
	}

	public String getTerrainType() {
		return terrainType;
	}

	public void setTerrainType(String type) {
		terrainType = type;
		if (uiPlugin != null)
		{
			terrain = uiPlugin.getTerrainImage(type);
		}
		updateView = true;
	}

	public void setTerrainImage(UIPlugin uiPlugin) {
		if (this.uiPlugin == null){
			this.uiPlugin = uiPlugin;
		}
		if(piece != null){
			piece.setImage(uiPlugin);
		}
		terrain = uiPlugin.getTerrainImage(terrainType);
		updateView = true;
	}

	@Override
	public void update(int positionsWidth, int positionsHeight) {
		//if(updateView){
			if(piece != null){
				if(terrain != null){
					BufferedImage pieceImage = piece.getImage();
				
					// create the new image, canvas size is the max. of both image sizes
					int width = BOARD_LENGTH / positionsWidth;
					int height = BOARD_LENGTH / positionsHeight;

					int w = Math.max(terrain.getWidth(), pieceImage.getWidth());
					int h = Math.max(terrain.getHeight(), pieceImage.getHeight());
					BufferedImage combined = new BufferedImage(w+ 50, h, BufferedImage.TYPE_INT_ARGB);
		
					// paint both images, preserving the alpha channels
					Graphics g = combined.getGraphics();
					g.drawImage(terrain, 0, 0, null);
					Image rescaledImage = pieceImage.getScaledInstance(terrain.getWidth(), terrain.getHeight(), Image.SCALE_SMOOTH);
					g.drawImage(rescaledImage, width / 2, 0, null);		
				
					ImageIcon positionImage = new ImageIcon(combined);
				
					Image img = positionImage.getImage();  
					Image newimg = img.getScaledInstance(width, height,  java.awt.Image.SCALE_SMOOTH);  
					
					ImageIcon newIcon = new ImageIcon(newimg); 

					setIcon(newIcon);
					setOpaque(true);
				}
				else{
					ImageIcon positionImage = new ImageIcon(piece.getImage());
					
					int width = BOARD_LENGTH / positionsWidth;
					int height = BOARD_LENGTH / positionsHeight;
					Image img = positionImage.getImage();  
					Image newimg = img.getScaledInstance(width, height,  java.awt.Image.SCALE_SMOOTH);  
					ImageIcon newIcon = new ImageIcon(newimg); 

					setIcon(newIcon);
					setOpaque(true);
				}
			}
			else{
				if(terrain != null){
					ImageIcon positionImage = new ImageIcon(terrain);
					
					int width = BOARD_LENGTH / positionsWidth;
					int height = BOARD_LENGTH / positionsHeight;
					Image img = positionImage.getImage();  
					Image newimg = img.getScaledInstance(width, height,  java.awt.Image.SCALE_SMOOTH);  
					ImageIcon newIcon = new ImageIcon(newimg); 

					setIcon(newIcon);
					setOpaque(true);
				}
				else{
					setIcon(null);
					setOpaque(true);
				}
			}
		//}
		updateView = false;
	}	
}

