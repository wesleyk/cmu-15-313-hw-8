package implementations.model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import interfaces.model.HexBoard;
import interfaces.model.Position;


/**
 *  This class defines the specifics for a board that has at most 6 adjacent
 *  locations.
 *  
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public class HexBoardImpl implements HexBoard{

	private int height;
	private int width;
	private Position[][] board;

	public HexBoardImpl(Position[][] board, int height, int width){
		this.height = height;
		this.width = width;
		this.board = board;
	}
	
	@Override
	public Position[][] getBoard() {
		return board;
	}

	@Override
	public String pointToUID(Point point) {
		//TODO Make this a more usable UID in the future
		return point.x + "," + point.y;
	}

	@Override
	public Point pointFromUID(String uid) {
		//TODO Make this a more usable UID in the future
		String[] splitUid = uid.split(",");
		return new Point(Integer.parseInt(splitUid[0]), Integer.parseInt(splitUid[1]));
	}

	@Override
	public void addNewPosition(Position pos) {
		
		Point point = pos.getPoint();
		
		if(point.x < 0 || point.x >= height)
			return;
		
		if(point.y < 0 || point.y >= width)
			return;
		
		if((point.x%2 == 0) && (point.y == width-1))
			return;
		
		if((point.x%2 == 1) && (point.y == 0))
			return;
		
		
		board[point.x][point.y] = pos;
		
	}

	@Override
	public List<Position> getPositionsWithPieces() {

		List<Position> ret = new ArrayList<Position>();
		
		for(int x = 0; x<height; x++){
			for(int y = 0; y<width; y++){
				if(board[x][y] != null && board[x][y].getPiece() != null)
					ret.add(board[x][y]);
			}
		}
		
		return ret;
	}

	@Override
	public List<Point> getAdjacent(Point srcPoint) {
	
		if(srcPoint.x < 0 || srcPoint.x >= height || srcPoint.y < 0 
				|| srcPoint.y >= width)
			return null;
		
		if(srcPoint.x % 2 == 0 && srcPoint.y % 2 == 0)
			return null;
		if(srcPoint.x % 2 == 1 && srcPoint.y % 2 == 1)
			return null;
		
		List<Point> ret = new ArrayList<Point>();
		
		int x, y;
		
		//Up 2
		x = srcPoint.x-2;
		y = srcPoint.y;
		
		if(x >= 0 && x < height && y >= 0 && y < width){
			ret.add(new Point(x, y));
		}

		//Up 1 Left 1
		x = srcPoint.x-1;
		y = srcPoint.y-1;
		
		if(x >= 0 && x < height && y >= 0 && y < width){
			ret.add(new Point(x, y));
		}
		
		//Up 1 Right 1
		x = srcPoint.x-1;
		y = srcPoint.y+1;
		
		if(x >= 0 && x < height && y >= 0 && y < width){
			ret.add(new Point(x, y));
		}
		
		//Down 1 Left 1
		x = srcPoint.x+1;
		y = srcPoint.y-1;
		
		if(x >= 0 && x < height && y >= 0 && y < width){
			ret.add(new Point(x, y));
		}
		
		//Down 1 Right 1
		x = srcPoint.x+1;
		y = srcPoint.y+1;
		
		if(x >= 0 && x < height && y >= 0 && y < width){
				ret.add(new Point(x, y));
		}
		
		//Down 2
		x = srcPoint.x+2;
		y = srcPoint.y;
		
		if(x >= 0 && x < height && y >= 0 && y < width){
			ret.add(new Point(x, y));
		}
		
		
		return ret;	
	}

	@Override
	public Position getPositionAt(Point point) {
		if(point.x < 0 || point.x >= height){
			return null;
		}
		
		if(point.y < 0 || point.y >= width){
			return null;
		}
		
		return board[point.x][point.y];
	}

	@Override
	public int getWidth() {
		return this.width;
	}

	@Override
	public int getHeight() {
		return this.height;
	}

}
