package implementations.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import interfaces.model.GameBoard;
import interfaces.model.GameState;
import interfaces.model.Player;
import interfaces.model.Position;

public class GameStateImpl implements GameState {

	private GameBoard board;
	private List<Player> players;
	

	public GameStateImpl(GameBoard board, List<Player> players){
		this.board = board;
		this.players = players;
	}
	
	
	public GameStateImpl(GameBoard gameBoard, Player[] players) {
		this.board = gameBoard;
		this.players = Arrays.asList(players);
	}


	@Override
	public GameBoard getGameBoard(){
		return board;
	}
	
	@Override
	public List<Position> getPositionsForPlayer(String playerName) {
		
		List<Position> ret = new ArrayList<Position>();
		
		if(board != null){
			for(Position p : board.getPositionsWithPieces()){
				if(p.getPiece().getPlayer().getName().equals(playerName)){
					ret.add(p);
				}
			}
		}
		
		return ret;
	}

	@Override
	public List<Position> getPositionsForTeam(int teamNumber) {
		
		List<Position> ret = new ArrayList<Position>();
			
		if(board != null){
			for(Position p : board.getPositionsWithPieces()){
				if(p.getPiece().getPlayer().getTeamNumber() == teamNumber){
					ret.add(p);
				}
			}
		}
			
		return ret;
	}

	
	@Override
	public List<Player> getPlayers() {
		return players;
	}
	
}
