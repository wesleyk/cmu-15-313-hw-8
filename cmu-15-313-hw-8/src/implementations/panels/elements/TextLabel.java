package implementations.panels.elements;

import javax.swing.JLabel;

import interfaces.view.elements.InfoElement;

public class TextLabel extends JLabel implements InfoElement {

	public TextLabel(String toDisplay) {
		super(toDisplay);
	}
	
}
