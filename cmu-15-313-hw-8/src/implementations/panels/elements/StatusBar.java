package implementations.panels.elements;

import interfaces.view.elements.InfoElement;

import java.awt.Color;

import javax.swing.JLabel;

public class StatusBar extends JLabel implements InfoElement {

	/**
	 * Creates a status bar that will be displayed on the info panel
	 * @param percentage Percentage of the status bar to fill
	 * @param label The StatusBar's label (e.g. "health")
	 */
	public StatusBar(String label, int percentage){
		super();
		if (percentage < 0 || percentage > 100){
			throw new IllegalArgumentException();
		}
		
		String statusString = label + ": [";
		
		for(int i = 0; i <= 100; i += 10){
			if (i < percentage){
				statusString += "=";
			}
			else{
				statusString += "-";
			}
		}
		
		statusString += "]";
		
		setText(statusString);
	}
	
}
