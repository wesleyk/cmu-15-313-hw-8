package implementations.panels;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import interfaces.view.GameFrame;
import interfaces.view.panels.InputPanel;

public class InputPanelImpl extends JPanel implements InputPanel {

	private JTextField inputField = new JTextField();
	private JButton submitButton = new JButton("Submit");
	
	public InputPanelImpl(ActionListener topFrame){
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder("Move Input"));
		
		submitButton.addActionListener(topFrame);
		inputField.addActionListener(topFrame);
		
		add(inputField, BorderLayout.CENTER);
		add(submitButton, BorderLayout.LINE_END);
	}
		
	public String getMoveText(){
		String out = inputField.getText();
		inputField.setText("");
		return out;
	}
	
}
