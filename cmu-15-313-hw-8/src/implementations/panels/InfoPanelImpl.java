package implementations.panels;

import javax.swing.JPanel;

import implementations.panels.elements.*;
import interfaces.view.elements.InfoElement;
import interfaces.view.panels.InfoPanel;

public class InfoPanelImpl extends JPanel implements InfoPanel {

	public InfoPanelImpl(){
		super();
		
		
	}
	
	public void addElement(InfoElement element) {
		if (element instanceof TextLabel){
			add((TextLabel)element);
		}
		else{
			add((StatusBar)element);
		}
	}

}
