package implementations.panels;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import implementations.model.PositionImpl;
import interfaces.model.GameBoard;
import interfaces.model.GameState;
import interfaces.model.Position;
import interfaces.plugins.UIPlugin;
import interfaces.view.panels.BoardPanel;

public class BoardPanelImpl extends JPanel implements BoardPanel {

	private UIPlugin uiPlugin;
	private Position[][] positions;
	private MouseListener topFrame;
	
	public BoardPanelImpl(MouseListener topFrame, GameState gameState, UIPlugin uiPlugin){
		this.uiPlugin = uiPlugin;
		this.topFrame = topFrame;
		
		setBorder(BorderFactory.createTitledBorder("Board"));
		setPreferredSize(new Dimension(550, 550));
		
		positions = gameState.getGameBoard().getBoard();
		
		setLayout(new GridLayout(positions[0].length, positions.length));
				
		for (Position[] positionRow : positions){
			for (Position position : positionRow){
				position.setTerrainImage(uiPlugin);
				
				((JLabel)position).addMouseListener(topFrame);
				position.update(positions[0].length, positions.length);
				add((PositionImpl)position);
			}
		}
	}

	public void update(GameState newState) {
		Position[][] newPositions = newState.getGameBoard().getBoard();
		
		if(newPositions.length != positions.length ||
				newPositions[0].length != positions[0].length){
			throw new IllegalArgumentException("Your gameplugin returned an incorrectly sized board");
		}
		
		for(int i = 0; i < newPositions.length; i++){
			for (int j = 0; j < newPositions[0].length; j++){
				positions[i][j].setPiece(newPositions[i][j].getPiece());
				positions[i][j].setTerrainType(newPositions[i][j].getTerrainType());
			}
		}
		
		for (Position[] positionRow : positions){
			for (Position position : positionRow){
				position.setTerrainImage(uiPlugin);
				
				position.update(positions[0].length, positions.length);
			}
		}
		
		remake();
	}
	
	private void remake()
	{
		this.removeAll();
		for (Position[] positionRow : positions){
			for (Position position : positionRow){
				position.setTerrainImage(uiPlugin);
				
				((JLabel)position).addMouseListener(topFrame);
				position.update(positions[0].length, positions.length);
				add((PositionImpl)position);
			}
		}
	}
	
}
