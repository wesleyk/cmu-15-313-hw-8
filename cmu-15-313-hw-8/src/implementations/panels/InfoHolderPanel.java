package implementations.panels;

import interfaces.model.GameState;
import interfaces.model.Position;
import interfaces.plugins.GamePlugin;
import interfaces.view.panels.InfoPanel;

import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;

public class InfoHolderPanel extends JPanel {

	private InfoPanel positionInfo;
	private InfoPanel stateInfo;
	
	public InfoHolderPanel(InfoPanel positionPanel, InfoPanel statePanel){
		setLayout(new GridLayout(1,2));
		
		positionInfo = positionPanel;
		stateInfo = statePanel;
		
		((JPanel) positionInfo).setBorder(BorderFactory.createTitledBorder("Position"));
		((JPanel) stateInfo).setBorder(BorderFactory.createTitledBorder("General"));
		
		add((JPanel) positionInfo);
		add((JPanel) stateInfo);
	}
	
	public void update(GamePlugin gamePlugin, GameState gameState){
		stateInfo = gamePlugin.makeGameInfoPanel(gameState);
		remake();
	}
	
	public void update(GamePlugin gamePlugin, Position position){
		positionInfo = gamePlugin.makePositionInfoPanel(position);
		remake();
	}
	
	private void remake()
	{
		this.removeAll();
		setLayout(new GridLayout(1,2));
		((JPanel) positionInfo).setBorder(BorderFactory.createTitledBorder("Position"));
		((JPanel) stateInfo).setBorder(BorderFactory.createTitledBorder("General"));
		
		add((JPanel) positionInfo);
		add((JPanel) stateInfo);
	}
}
