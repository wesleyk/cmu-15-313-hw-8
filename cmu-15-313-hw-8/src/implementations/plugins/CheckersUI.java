package implementations.plugins;

import interfaces.plugins.UIPlugin;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * An implementation of the UIPlugin interface with Checkers
 * 
 * @author Peter Xiao (phx)
 * @author Wesley Kim (wesleyk)
 */
public class CheckersUI implements UIPlugin {
	public static String blackPiece = "Black";
	public static String blackKingPiece = "BlackKing";
	public static String blackTerrain = "blackTerrain";
	public static String redPiece = "Red";
	public static String redKingPiece = "RedKing";
	public static String redTerrain = "redTerrain";

	private final String path = "./src/implementations/plugins/";
	@Override
	public BufferedImage getTerrainImage(String terrainType) {
		BufferedImage img = null;
		try {
			if(terrainType.equals(blackTerrain)) {
				img = ImageIO.read(new File(path + "blackTerrain.png"));
			}
			else {
				img = ImageIO.read(new File(path + "redTerrain.png"));
			}
		} catch (Exception e) {
		}
		
		return img;
	}

	@Override
	public BufferedImage getPieceImage(String pieceType) {
		BufferedImage img = null;
		try {
			if(pieceType.equals(blackPiece)) {
				img = ImageIO.read(new File(path + "blackPiece.png"));
			}
            else if(pieceType.equals(blackKingPiece)) {
                img = ImageIO.read(new File(path + "blackKing.png"));
            }
            else if(pieceType.equals(redKingPiece)) {
                img = ImageIO.read(new File(path + "redKing.png"));
            }
			else {
				img = ImageIO.read(new File(path + "redPiece.png"));
			}
		} catch (Exception e) {
		}
		
		return img;
	}

}
