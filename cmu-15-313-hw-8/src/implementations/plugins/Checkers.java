package implementations.plugins;

import implementations.model.PieceImpl;
import implementations.model.PlayerImpl;
import implementations.model.PositionImpl;
import implementations.panels.InfoPanelImpl;
import implementations.panels.InstructionsPanelImpl;
import implementations.panels.elements.TextLabel;
import interfaces.model.*;
import interfaces.plugins.GamePlugin;
import interfaces.view.elements.InfoElement;
import interfaces.view.panels.InfoPanel;
import interfaces.view.panels.InstructionsPanel;

import java.awt.*;
import java.util.ArrayList;

public class Checkers implements GamePlugin {

    private int maxTeams;
    private int minTeams;
    private int maxPlayers;
    private int minPlayers;
    private int maxPerTeam;
    private int minPerTeam;
    private String[] computerTypes;

    public static int boardSize = 8;
    public static String blackPiece = "Black";
    public static String blackKingPiece = "BlackKing";
    public static String blackTerrain = "blackTerrain";
    public static String redPiece = "Red";
    public static String redKingPiece = "RedKing";
    public static String redTerrain = "redTerrain";
    public static boolean isP0Turn = true;

    public static Player p0 = new PlayerImpl("PlayerZero", 0);
    public static Player p1 = new PlayerImpl("PlayerOne", 1);

    public Checkers() {
        setMaxTeams(2);
        setMinTeams(2);
        setMaxPlayers(2);
        setMinPlayers(2);
        setMaxPerTeam(1);
        setMinPerTeam(1);
//    	String[] types = {"Autobot", "Nobot", "CheckersMaster"};
        String[] types = {};
        setComputerTypes(types);

    }

    @Override
    public int getMaxTeams() {
        return maxTeams;
    }

    @Override
    public int getMinTeams() {
        return minTeams;
    }

    @Override
    public int getMaxPlayers() {
        return maxPlayers;
    }

    @Override
    public int getMinPlayers() {
        return minPlayers;
    }

    @Override
    public int getMaxPerTeam() {
        return maxPerTeam;
    }

    @Override
    public int getMinPerTeam() {
        return minPerTeam;
    }

    @Override
    public String[] computerTypes() {
        return computerTypes;
    }

    @Override
    public void setMaxTeams(int maxTeams) {
        this.maxTeams = maxTeams;

    }

    @Override
    public void setMinTeams(int minTeams) {
        this.minTeams = minTeams;

    }

    @Override
    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;

    }

    @Override
    public void setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;

    }

    @Override
    public void setMaxPerTeam(int maxPerTeam) {
        this.maxPerTeam = maxPerTeam;

    }

    @Override
    public void setMinPerTeam(int minPerTeam) {
        this.minPerTeam = minPerTeam;

    }

    @Override
    public void setComputerTypes(String[] computerTypes) {
        this.computerTypes = computerTypes;
    }

    @Override
    public InfoPanel makePositionInfoPanel(Position position) {
        InfoPanel retPanel = new InfoPanelImpl();
        InfoElement element;
        if (position.getPiece() != null) {
            element = new TextLabel("" + position.getPiece().getPieceType() +
                                            " " + position.getPoint().toString());
        } else {
            element = new TextLabel("" + position.getPoint().toString());
        }
        retPanel.addElement(element);
        return retPanel;
    }

    @Override
    public InfoPanel makeGameInfoPanel(GameState currentState) {
        InfoPanel retPanel = new InfoPanelImpl();
        if (isP0Turn) {
            retPanel.addElement(new TextLabel("Player 1's turn"));
        } else {
            retPanel.addElement(new TextLabel("Player 2's turn"));
        }
        return retPanel;
    }

    @Override
    public InstructionsPanel makeInstructionsPanel(GameState currentState) {
        String instructions = "To make a move, type in the " +
                "coordinates of the sequence of moves that will be made for a given piece. " +
                "Use syntax as following: x1,y1;x2,y2;x3,y3... for example: 1,1;2,2;3,3";

        return new InstructionsPanelImpl(instructions);
    }

    @Override
    public Position[][] setupBoard(Player[] players) {
        System.out.println("setupBoard1");
        Position[][] newBoard = new Position[boardSize][boardSize];
        boolean isBlackTerrain = false;

        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                Point curPoint = new Point(i, j);

                newBoard[i][j] = new PositionImpl(null, new Point(i, j), "None");

                //only assign pieces to black spots
                if (isBlackTerrain) {
                    newBoard[i][j].setTerrainType(blackTerrain);
                    if (i < 3) {
                        newBoard[i][j].setPiece(new PieceImpl(p0, blackPiece));
                    } else if (boardSize - i <= 3) {
                        newBoard[i][j].setPiece(new PieceImpl(p1, redPiece));
                    }
                } else {
                    newBoard[0][0].setTerrainType(redTerrain);
                }

                isBlackTerrain = !isBlackTerrain;
            }
            isBlackTerrain = !isBlackTerrain;
        }
        return newBoard;
    }

    @Override
    public GameBoard runTurn(String commandInput, GameState currentState) {
        GameBoard currentBoard = currentState.getGameBoard();
        ArrayList<Point> moves = parseCommandInput(commandInput);

        //invalid move
        if (moves.size() < 2) {
            return currentBoard;
        }

        if (!isAllOddSquare(moves)) {
            return currentBoard;
        }

        Point startPoint = moves.get(0);
        Position startPos = currentBoard.getPositionAt(startPoint);
        Piece startPiece = startPos.getPiece();

        //remove piece at starting point
        startPos.setPiece(null);
        currentBoard.addNewPosition(startPos);

        //invalid starting piece
        if (startPiece == null || (isP0Turn && !startPiece.getPlayer().equals(p0)
                || !isP0Turn && !startPiece.getPlayer().equals(p1))) {
            return currentBoard;
        }

        Point nextPoint = moves.get(1);
        for (int i = 2; i <= moves.size(); i++) {
            Point midPoint = new Point((startPoint.x + nextPoint.x) / 2,
                                       (startPoint.y + nextPoint.y) / 2);
            Position midPos = currentBoard.getPositionAt(midPoint);
            midPos.setPiece(null); //remove piece at the position
            currentBoard.addNewPosition(midPos);
            if (i < moves.size()) {
                startPoint = nextPoint;
                nextPoint = moves.get(i);
            }
        }

        //update final position
        Position finalPos = currentBoard.getPositionAt(nextPoint);
        finalPos.setPiece(startPiece);
        currentBoard.addNewPosition(finalPos);

        isP0Turn = !isP0Turn;
        return currentBoard;
    }

    @Override
    public Player whosTurn(GameState currentState) {
        if (isP0Turn) {
            return p0;
        }
        return p1;
    }

    @Override
    public int[] gameOver(GameState currentState) {
        Position[][] board = currentState.getGameBoard().getBoard();
        boolean p0Won = true;
        boolean p1Won = true;
        //run through board, looking to see if there are only pieces of one player
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (!p0Won && !p1Won) {
                    return null;
                }
                if (board[i][j].getPiece() == null) {
                    continue;
                }
                Player cur = board[i][j].getPiece().getPlayer();
                if (cur != null && cur.equals(p0)) {
                    p1Won = false;
                } else if (cur != null && cur.equals(p1)) {
                    p0Won = false;
                }
            }
        }

        int[] p1WonArr = new int[1];
        p1WonArr[0] = 2;
        int[] p0WonArr = new int[1];
        p0WonArr[0] = 1;
        if (p1Won) {
            return p1WonArr;
        } else {
            return p0WonArr;
        }
    }

    @Override
    public GameBoard cleanUp(GameState currentState) {
        // unneeded in checkers
        return currentState.getGameBoard();
    }

    public ArrayList<Point> parseCommandInput(String commandInput) {
        String[] hops = commandInput.split(";");
        ArrayList<Point> moves = new ArrayList<Point>();

        for (String hop : hops) {
            String[] coord = hop.split(",");
            if (coord.length != 2) {
                throw new IllegalArgumentException("Incorrectly formatted string.");
            }
            moves.add(new Point(Integer.parseInt(coord[0]), Integer.parseInt(coord[1])));
        }
        return moves;
    }

    public boolean isAllOddSquare(ArrayList<Point> points) {
        for (Point myPoint : points) {
            if ((myPoint.x + myPoint.y) % 2 == 0) {
                return false;
            }
        }
        return true;
    }
}
