package implementations.plugins;

import java.awt.Point;
import java.util.List;

import javax.swing.JPanel;

import implementations.model.*;
import implementations.panels.*;
import implementations.panels.elements.TextLabel;
import interfaces.model.*;
import interfaces.plugins.GamePlugin;
import interfaces.view.elements.InfoElement;
import interfaces.view.panels.*;

public class PokeRacer implements GamePlugin {

	private int maxTeams;
	private int minTeams;
	private int maxPlayers;
	private int minPlayers;
	private int maxPerTeam;
	private int minPerTeam;
	private String[] computerTypes;
	
	private int turn = 0;
	Player[] order;

	//TODO: add a simple bot
	public PokeRacer() {
        setMaxTeams(2);
        setMinTeams(2);
    	setMaxPlayers(2);
    	setMinPlayers(2);
    	setMaxPerTeam(1);
    	setMinPerTeam(1);
//    	String[] types = {"Autobot", "Nobot", "CheckersMaster"};
    	String[] types = {};
    	setComputerTypes(types);
    	
	}

	@Override
	public int getMaxTeams() {
		return maxTeams;
	}

	@Override
	public int getMinTeams() {
		return minTeams;
	}

	@Override
	public int getMaxPlayers() {
		return maxPlayers;
	}

	@Override
	public int getMinPlayers() {
		return minPlayers;
	}

	@Override
	public int getMaxPerTeam() {
		return maxPerTeam;
	}

	@Override
	public int getMinPerTeam() {
		return minPerTeam;
	}

	@Override
	public String[] computerTypes() {
		return computerTypes;
	}

	@Override
	public void setMaxTeams(int maxTeams) {
		this.maxTeams = maxTeams;
		
	}

	@Override
	public void setMinTeams(int minTeams) {
		this.minTeams = minTeams;
	}

	@Override
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
		
	}

	@Override
	public void setMinPlayers(int minPlayers) {
		this.minPlayers = minPlayers;
		
	}

	@Override
	public void setMaxPerTeam(int maxPerTeam) {
		this.maxPerTeam = maxPerTeam;
		
	}

	@Override
	public void setMinPerTeam(int minPerTeam) {
		this.minPerTeam = minPerTeam;
		
	}

	@Override
	public void setComputerTypes(String[] computerTypes) {
		this.computerTypes = computerTypes;
	}

	@Override
	public InfoPanel makePositionInfoPanel(Position position) {
		InfoPanel retPanel = new InfoPanelImpl();
		InfoElement element;
		if(position.getPiece() != null)
			element = new TextLabel(""+position.getPiece().getPieceType());
		else
			element = new TextLabel("null");
		retPanel.addElement(element);
		return retPanel;
	}

	@Override
	public InfoPanel makeGameInfoPanel(GameState currentState) {
		InfoPanel retPanel = new InfoPanelImpl();
		retPanel.addElement(new TextLabel(""+turn));
		return retPanel;
	}

	@Override
	public InstructionsPanel makeInstructionsPanel(GameState currentState) {
		GameBoard board = currentState.getGameBoard();
		String retString = "Where am I?";
		
		for(int i=0;i<board.getHeight();i++)
		{
			for(int j=0;j<board.getWidth();j++)
			{
				
				if(board.getPositionAt(new Point(i,j)).getPiece() != null && 
						board.getPositionAt(new Point(i,j)).getPiece().getPieceType() == "Trainer")
					retString = "I'm no the board!";
			}
		}
		
		return new InstructionsPanelImpl(retString);
	}

	@Override
	public Position[][] setupBoard(Player[] players) {
		Position[][] board = new PositionImpl[9][9];
		for(int i = 0;i<9;i++)
		{
			for(int j = 0;j<9;j++)
			{
				board[i][j] = new PositionImpl(null,new Point(i,j),"None");
			}
		}
		board[0][0].setTerrainType("Goal");
		board[7][8].setPiece(new PieceImpl(players[0], "Trainer"));
		board[8][7].setPiece(new PieceImpl(players[1], "Rival"));
		
		order = new Player[3];
		order[players[0].getTeamNumber()-1]=players[0];//So team 1 is the first to go
		order[players[1].getTeamNumber()-1]=players[1];
		order[2]=null;
		return board;
	}

	@Override
	public GameBoard runTurn(String commandInput, GameState currentState) {
		System.out.println("turn1");
		GameBoard board = currentState.getGameBoard();
		Point posChange = new Point();
		
		Position[][] grid = board.getBoard();
		
		System.out.println("Current Player : " + whosTurn(currentState).getName());
		Position moverPos = currentState.getPositionsForPlayer(whosTurn(currentState).getName()).get(0);
		Piece mover = moverPos.getPiece();
		Point orig = moverPos.getPoint();
		
		if(commandInput.equals("left"))
			posChange = new Point(0,-1);
		else if(commandInput.equals("right"))
			posChange = new Point(0,1);
		else if(commandInput.equals("up"))
			posChange = new Point(-1,0);
		else if(commandInput.equals("down"))
			posChange = new Point(1,0);
		else if(commandInput.equals(""))
		{
			turn++;
			return board;	
		}
		else return null;
		
		Point targetLoc = new Point(posChange.x+orig.x,posChange.y+orig.y);
		
		if(board.getPositionAt(targetLoc).getPiece() != null)
			return null;
		
		//set new location
		if(targetLoc.x == 0 && targetLoc.y == 0)
			grid[targetLoc.x][targetLoc.y] = new PositionImpl(mover,targetLoc,"Goal");
		else
			grid[targetLoc.x][targetLoc.y] = new PositionImpl(mover,targetLoc,null);
		grid[orig.x][orig.y] = new PositionImpl(null,orig,null);
		
		GameBoard newBoard = new SquareBoardImpl(grid, grid.length,grid[0].length);
		
		
		for(int i=0;i<grid.length;i++)
		{
			System.out.println();
			for(int j=0;j<grid.length;j++)
			{
				System.out.print(grid[i][j].getPiece());
			}
			
		}
		turn++;
		return newBoard;
	}

	@Override
	public Player whosTurn(GameState currentState) {
		return order[turn%3];
	}

	@Override
	public int[] gameOver(GameState currentState) {
		GameBoard board = currentState.getGameBoard();
		Position goal = board.getPositionAt(new Point(0,0));
				
		if(goal.getPiece() == null)
		{
			return null;
		}
		
		if(goal.getPiece().getPlayer() != null)
		{
			int[] retval = {goal.getPiece().getPlayer().getTeamNumber()};
			return retval;
		}
		
		return new int[0];
	}

	/*This will be useful when I add pokemon that move around the map and interfere with the
	 * trainer race
	 */
	@Override
	public GameBoard cleanUp(GameState currentState) {
		turn++;
		return currentState.getGameBoard();
	}

}
