package implementations.plugins;

import interfaces.model.GameBoard;
import interfaces.model.GameState;
import interfaces.model.Player;
import interfaces.model.Position;
import interfaces.plugins.GamePlugin;
import interfaces.view.panels.InfoPanel;
import interfaces.view.panels.InstructionsPanel;

public class Monopoly implements GamePlugin {

	private int maxTeams;
	private int minTeams;
	private int maxPlayers;
	private int minPlayers;
	private int maxPerTeam;
	private int minPerTeam;
	private String[] computerTypes;

	public Monopoly() {
        setMaxTeams(1);
        setMinTeams(1);
    	setMaxPlayers(2);
    	setMinPlayers(1);
    	setMaxPerTeam(2);
    	setMinPerTeam(1);
    	String[] types = {"Bot", "Dumbot"};
    	setComputerTypes(types);
	}

	@Override
	public int getMaxTeams() {
		return maxTeams;
	}

	@Override
	public int getMinTeams() {
		return minTeams;
	}

	@Override
	public int getMaxPlayers() {
		return maxPlayers;
	}

	@Override
	public int getMinPlayers() {
		return minPlayers;
	}

	@Override
	public int getMaxPerTeam() {
		return maxPerTeam;
	}

	@Override
	public int getMinPerTeam() {
		return minPerTeam;
	}

	@Override
	public String[] computerTypes() {
		return computerTypes;
	}

	@Override
	public void setMaxTeams(int maxTeams) {
		this.maxTeams = maxTeams;
		
	}

	@Override
	public void setMinTeams(int minTeams) {
		this.minTeams = minTeams;
		
	}

	@Override
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
		
	}

	@Override
	public void setMinPlayers(int minPlayers) {
		this.minPlayers = minPlayers;
		
	}

	@Override
	public void setMaxPerTeam(int maxPerTeam) {
		this.maxPerTeam = maxPerTeam;
		
	}

	@Override
	public void setMinPerTeam(int minPerTeam) {
		this.minPerTeam = minPerTeam;
		
	}

	@Override
	public void setComputerTypes(String[] computerTypes) {
		this.computerTypes = computerTypes;
	}

	@Override
	public InfoPanel makePositionInfoPanel(Position position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InfoPanel makeGameInfoPanel(GameState currentState) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InstructionsPanel makeInstructionsPanel(GameState currentState) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Position[][] setupBoard(Player[] players) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GameBoard runTurn(String commandInput, GameState currentState) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Player whosTurn(GameState currentState) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] gameOver(GameState currentState) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GameBoard cleanUp(GameState currentState) {
		// TODO Auto-generated method stub
		return null;
	}

}
